using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Net.Sockets;
using Newtonsoft.Json;
using RudeAI;
using RudeAI.Car;
using RudeAI.GI;

namespace RudeAI 
{
	public class Bot 
	{
		// huom. http://www.reddit.com/r/HWO/comments/239jir/about_mono_compilers/
	    // http://www.reddit.com/r/HWO/comments/23dyu7/gametick_in_carpositions_not_there/

		internal LaneSwitcher Switcher = new LaneSwitcher();
		internal SpeedHelper SHelper = new SpeedHelper();

		public static void Main(string[] args) 
		{
		    string host = args[0];
	        int port = int.Parse(args[1]);
	        string botName = args[2];
			string botKey = args[3];

			// Useita botteja varten testausnimi
			//botName += "-" + new Random().Next(1,99);

			Console.WriteLine("Connecting to " + host + ":" + port + " as " + botName + "/" + botKey);

			using(TcpClient client = new TcpClient(host, port)) {
				NetworkStream stream = client.GetStream();
				StreamReader reader = new StreamReader(stream);
				StreamWriter writer = new StreamWriter(stream);
				writer.AutoFlush = true;			

				new Bot(reader, writer, new Join(botName, botKey));
			}
		}

		private StreamWriter writer;

		Bot(StreamReader reader, StreamWriter writer, Join join) {
			this.writer = writer;
			string line;

			RudeAI.Car.CarOperations carOperations = new CarOperations();
			RudeAI.GI.Data gameData = new RudeAI.GI.Data();
			TurboData turboData = new TurboData ();

			// Throttlemuuttujat
			double throttle = 0;
			double engineFactor = 1;

			double cornerForce = 0;
			double safeCornerForce = 0.25;
			double topCornerForce = 0;

			// edellinen sijainti
			double previousPosition = 0;
			int currentLap = 0;

			// Etäisyysmuuttujat
			double distanceToNextCurve = 0;
			double currentPiecelength = 0;
			double previousPieceLength = 0;
			double longeststraightdistance = 0;
			double distancetonextcar = 0;

			// Indeximuuttujat
			int pieceIndex = 0;
			int longeststraightIndex = 0;

			double? nextCurveAngle = null;
			int? nextCurveRadius = null;

			// Nopeusmuuttujat
			double speed = 0;
			double previousSpeed = 0;
			double topspeed = 0;
			double acceleration = 0;

			// Drift-kulmamuuttujat
			double topangle = 0;

			// Turbomuuttujat
			bool turboAvailable = false;
			bool turboinuse = false;

			bool laneChangeMessageSentForCurrentPiece = false;	

			send(join);

			while((line = reader.ReadLine()) != null) {
				MsgWrapper msg = JsonConvert.DeserializeObject<MsgWrapper>(line);
				switch(msg.msgType) {
				case "carPositions":

					Console.Out.WriteLine ("Throttle: " + throttle);
					Console.Out.WriteLine ("Distance to next curve: " + distanceToNextCurve);
					Console.Out.WriteLine ("Speed: " + speed);
					Console.Out.WriteLine ("Top speed: " + topspeed);
					Console.Out.WriteLine ("Acceleration: " + acceleration);
					Console.Out.WriteLine ("Corner force: " + cornerForce);
					Console.Out.WriteLine ("Safe corner force: " + safeCornerForce);
					Console.Out.WriteLine ("Top angle: " + topangle);
					Console.Out.WriteLine ("Distance to next car: " + distancetonextcar);

					// Haetaan kaikkien autojen sijaintitiedot
					List<CarPosition> carPositionData = JsonConvert.DeserializeObject<List<CarPosition>> (msg.data.ToString ());
					var myCarData = carPositionData.First (x => x.id.name == join.name);

					// Lasketaan etäisyys samalla kaistalla edelläolevaan autoon
					distancetonextcar = Switcher.GetDistanceToNextCar (carPositionData, join.name, gameData.race.track, longeststraightdistance);

					// Asetetaan edellisen kappaleen pituus
					if (pieceIndex != myCarData.piecePosition.pieceIndex) {
						previousPieceLength = currentPiecelength;
					}

					// Käytetään turbo jos ollaan radan pisimmän suoran alussa ja edessä ei ole ketään hidastamassa
					if (turboAvailable && !turboinuse && 
						(longeststraightIndex == myCarData.piecePosition.pieceIndex) && 
						(distancetonextcar > 80 || distancetonextcar == 0)) {
						send(new Turbo());
						turboAvailable = false;
						break;
					}

					if (!turboinuse && Math.Abs (myCarData.angle) >= topangle) {
						topangle = Math.Abs (myCarData.angle);
					}

					// Päivitetään turvallisen keskipakovoiman raja-arvoa edellisen kierroksen tulosten mukaan
					if (myCarData.piecePosition.lap > 0 && myCarData.piecePosition.lap != currentLap) {
						var temp = topangle / (55);
						if (temp > 1) {
							safeCornerForce = safeCornerForce * 0.95;
						} else {
							safeCornerForce = safeCornerForce * (1 + ((1 - temp) * 0.2));
						}
						topangle = 0;
					}
					currentLap = myCarData.piecePosition.lap;

					// Lasketaan nykyisen kappaleen pituus
					currentPiecelength = SHelper.DeterminePieceLength (myCarData.piecePosition.pieceIndex, myCarData.piecePosition.lane.startLaneIndex, myCarData.piecePosition.lane.endLaneIndex, gameData);
		
					// Lasketaan etäisyys seuraavaan kappaleeseen
					distanceToNextCurve = this.SHelper.DetermineDistanceToNextCurve (myCarData.piecePosition.pieceIndex, 
						myCarData.piecePosition.inPieceDistance, 
						gameData, ref nextCurveAngle, ref nextCurveRadius);

					// Päätellään nykyinen nopeus ja sijoitetaan nykyinen sijainti
					previousSpeed = speed;
					speed = this.SHelper.DetermineSpeed (previousPosition, myCarData.piecePosition.inPieceDistance, previousPieceLength);
					if (speed >= topspeed && speed < 20)
						topspeed = speed;
					acceleration = speed - previousSpeed;
					previousPosition = myCarData.piecePosition.inPieceDistance;

					// Päätellään throttlea varten moottorikerroin kiihtyvyydestä
					if (!turboinuse && speed > 0 && acceleration > 0 && throttle > 0) {
						var tempengineFactor = throttle / acceleration;
						if (tempengineFactor < engineFactor)
							engineFactor = Math.Abs (tempengineFactor);
					}

					var piece = gameData.race.track.pieces [myCarData.piecePosition.pieceIndex];
					var lane = gameData.race.track.lanes [myCarData.piecePosition.lane.endLaneIndex];
					var laneDistance = lane.distanceFromCenter;
					if (nextCurveAngle < 0)
						laneDistance = laneDistance * -1;

					// Päätellään maksiminopeus seuraavassa kulmassa
					var maxspeed = Math.Sqrt (safeCornerForce * (nextCurveRadius ?? 0 - laneDistance));
					Console.Out.WriteLine ("Max speed in corner: " + maxspeed);
					Console.WriteLine ("\nCarPositions: " + msg.data.ToString () + "\n");

					// Nopeudenpäättely suoralla
					if (distanceToNextCurve > 0 && distanceToNextCurve > (((speed - maxspeed) / 0.2) * speed) * 1.2) {
						throttle = 1;
					} 

					// Päätellään mutkaan hidastettavalla pätkällä käytettävä throttle
					else if (distanceToNextCurve > 0) {
						if (speed > maxspeed) {
							throttle = 0.1;
						} else {
							throttle = Math.Abs (acceleration * engineFactor);
							if (turboinuse)
								throttle = throttle / turboData.turboFactor;
						}

						// Vittuiluturbon käyttö jos mutkassa on ruuhkaa
						if (distancetonextcar < 80 && distancetonextcar != 0) {
							if (turboAvailable && !turboinuse) {
								send(new Turbo());
								turboAvailable = false;
							}
							throttle = 1;
						}
						if (turboinuse && distancetonextcar < 60 && distancetonextcar != 0) {
							throttle = 1;
						}
					} 

					// Päätellään mutkan sisällä käytettävä throttle
					else {
						cornerForce = this.SHelper.DetermineCornerForce (myCarData.piecePosition.pieceIndex, myCarData.piecePosition.lane.endLaneIndex, gameData, speed);
						if (cornerForce > topCornerForce)
							topCornerForce = cornerForce;
						if (cornerForce > safeCornerForce) {
							if (distancetonextcar < 60 && distancetonextcar != 0) {
								throttle = 1;
							}
							throttle = 0.1;
						} else {
							throttle = safeCornerForce / cornerForce;
						}

						if (distancetonextcar < 80 && distancetonextcar != 0) {
							throttle = 1;							
						}

						if (Math.Abs (myCarData.angle) > (55)) {
							throttle = 0;
						}
					}

					// Päätellään mahdollinen kaistanvaihto
					if(pieceIndex != myCarData.piecePosition.pieceIndex)
					{
						laneChangeMessageSentForCurrentPiece = false;
						pieceIndex = myCarData.piecePosition.pieceIndex;
					}

					if(!laneChangeMessageSentForCurrentPiece)
					{
						laneChangeMessageSentForCurrentPiece = this.SwitchLane(carPositionData, myCarData.id.name, gameData.race.track, speed);
						if(laneChangeMessageSentForCurrentPiece)
						{
							break;
						}
					}

					send(new Throttle(throttle));
					break;
				case "join":
					Console.WriteLine("Joined");
					send(new Ping());
					break;
				case "gameInit":
					Console.WriteLine ("Race init");		
					previousPosition = 0;
					turboAvailable = false;
					turboinuse = false;
					previousPieceLength = 0;
					previousSpeed = 0;
					pieceIndex = 0;
					currentLap = 0;

					// Päätellään edellisestä kisasta uudet muuttujat jos tarpeen
					if (topangle > 0) {
						var temp = topangle / (55);
						if (temp > 1) {
							safeCornerForce = safeCornerForce * 0.95;
						} 
						topangle = 0;
					}
					gameData = JsonConvert.DeserializeObject<RudeAI.GI.Data>(msg.data.ToString());
					longeststraightdistance = SHelper.GetLongestStraighth(gameData, ref longeststraightIndex);
					Console.WriteLine ("Track: " + gameData.race.track.name);
					Console.WriteLine ("Data: " + msg.data.ToString());
					Console.WriteLine ("Longest straight: " + longeststraightdistance);
					Console.WriteLine ("Longest straight index: " + longeststraightIndex);
					this.Switcher.Initialize(gameData.race.track);
					send(new Ping());
					break;
				case "gameEnd":
					Console.WriteLine("Race ended");
					send(new Ping());
					break;
				case "crash":
					Console.WriteLine ("Crashed");
					send(new Ping());
					break;
				case "spawn":
					turboinuse = false;
					previousPosition = 0;
					previousPieceLength = 0;
					previousSpeed = 0;
					safeCornerForce = safeCornerForce * 0.95;
					Console.WriteLine("Spawned");
					send(new Throttle(1));
					break;
				case "dnf":
					Console.WriteLine ("\nDNF: " + msg.data.ToString () + "\n");
					send (new Ping());
					break;
				case "gameStart":
					Console.WriteLine("Race starts");
					send(new Throttle(1));
					break;
				case "turboAvailable":
					Console.WriteLine ("Turbo available");
					turboData = JsonConvert.DeserializeObject<TurboData>(msg.data.ToString());
					turboAvailable = true;
					send (new Ping());
					break;
				case "turboStart":
					Console.WriteLine ("Turbo start");
					turboinuse = true;
					send (new Ping());
					break;
				case "turboEnd":
					Console.WriteLine ("Turbo end");
					turboinuse = false;
					send (new Ping());
					break;
				default:
					send(new Ping());
					break;
				}
			}
		}

		private void send(SendMsg msg) {
			writer.WriteLine(msg.ToJson());
		}

		private bool SwitchLane(List<CarPosition> carPositionData, string myCarName, Track trackData, double speed)
		{
			var changeTo = this.Switcher.Change(carPositionData, myCarName, trackData, speed);
			if(changeTo != null && !changeTo.Equals(LaneChange.NoChange))
			{
				send(new SwitchLane(changeTo));
				Console.Out.WriteLine("Change lane to " + changeTo);
				return true;
			}

			return false;
		}
	}

	class MsgWrapper {
	    public string msgType;
	    public Object data;

	    public MsgWrapper(string msgType, Object data) {
	    	this.msgType = msgType;
	    	this.data = data;
	    }
	}

	class JoinMsgWrapper {
		public string msgType;
		public Object data;
		public string trackName;
		public int carCount;
		
		public JoinMsgWrapper(string msgType, Object data, string trackName, int carCount) {
			this.msgType = msgType;
			this.data = data;
			this.trackName = trackName;
			this.carCount = carCount;
		}
	}

	abstract class SendMsg {
		public string ToJson ()
		{
			string result = JsonConvert.SerializeObject (new MsgWrapper (this.MsgType (), this.MsgData ()));
			if (this.MsgType().Equals("joinRace") || this.MsgType ().Equals("createRace")) {
				Console.WriteLine (result);
			}
			return result;
		}
		protected virtual Object MsgData() {
	        return this;
	    }

	    protected abstract string MsgType();
	}

	class Join: SendMsg {
		//public BotId botId;
		//public string password;
		public string name;
		public string key;
		//public string trackName;
		//public int carCount;

		public Join(string name, string key) {
			//this.botId = new BotId() { name = name, key = key};
			this.name = name;
			this.key = key;
			// tänhetkiset radat:
			//this.trackName = "keimola"; 
			//this.trackName = "germany"; 
			//this.trackName = "usa"; 
			//this.trackName = "france"; 
			//this.password = "foobar";
			// jotain ongelmaa vielä jos laittaa > 1 autoa
			//this.carCount = 2;
		}

		protected override string MsgType() {
			//return "joinRace";
			return "join";
		}
	}

	class CreateRace: SendMsg {
		public BotId botId;
		public string trackName;
		public string password;
		public int carCount;

		public CreateRace (string name, string key)
		{
			this.botId = new BotId() { name = name, key = key};
			this.trackName = "usa";
			this.password = "foobar";
			this.carCount = 3;
		}

		protected override string MsgType()	{
			return "createRace";
		}
	}

	class BotId{
		public string name;
		public string key;
	}

	class SwitchLane: SendMsg {
		public string value;

		public SwitchLane (string switchLaneTo)
		{
			this.value = switchLaneTo;			
		}

		protected override Object MsgData() {
			return this.value;
		}

		protected override string MsgType() {
			return "switchLane";
		}
	}

	class Turbo: SendMsg {
		public string value;

		public Turbo ()
		{
			this.value = "TURBO BOOST";
		}

		protected override Object MsgData() {
			return this.value;
		}
		
		protected override string MsgType() {
			return "turbo";
		}
	}

	class Ping: SendMsg {
		protected override string MsgType() {
			return "ping";
		}
	}

	class Throttle: SendMsg {
		public double value;
		public int? gameTick;

		public Throttle (double value)
		{
			if (value >= 1) {
				value = 1;
			}
			if (value <= 0) {
				value = 0;
			}
			this.value = value;
		}

		public Throttle (double value, int gameTick)
		{
			this.value = value;
			this.gameTick = gameTick;
		}

		protected override Object MsgData() {
			return this.value;
		}

		protected override string MsgType() {
			return "throttle";
		}
	}
}

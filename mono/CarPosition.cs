using System;
using System.Collections.Generic;

namespace RudeAI 
{
	public class Id
	{
		public string name { get; set; }
		public string color { get; set; }
	}
	
	public class Lane
	{
		public int startLaneIndex { get; set; }
		public int endLaneIndex { get; set; }
	}
	
	public class PiecePosition
	{
		public int pieceIndex { get; set; }
		public double inPieceDistance { get; set; }
		public Lane lane { get; set; }
		public int lap { get; set; }
	}
	
	public class CarPosition
	{
		public Id id { get; set; }
		public double angle { get; set; }
		public PiecePosition piecePosition { get; set; }
	}
}


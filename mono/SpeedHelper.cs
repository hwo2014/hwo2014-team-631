using System;

namespace RudeAI
{
	public class SpeedHelper
	{
		// Päätellään pisimmän suoran pituus ja indexi
		public double GetLongestStraighth(RudeAI.GI.Data gameData, ref int index)
		{
			double length = 0;
			double result = 0;
			for (int i = 0; i < gameData.race.track.pieces.Count; i++) {
				if (gameData.race.track.pieces [i].angle == null) {
					length = DetermineStraightDistance (i, gameData);
					if (length > result) {
						result = length;
						index = i;
					}
					length = 0;
				}
			}

			return result;
		}

		// Päätellään etäisyys seuraavaan kulmaan indexin alusta
		private double DetermineStraightDistance(int pieceIndex, RudeAI.GI.Data gameData)
		{
			double distanceToNextCurve = 0;
			for (int i = pieceIndex; i < gameData.race.track.pieces.Count; i++)
			{
				if (gameData.race.track.pieces[i].angle != null) {
					return distanceToNextCurve;
				}
				distanceToNextCurve += gameData.race.track.pieces[i].length;
			}
			for (int i = 0; i < pieceIndex; i++) {
				if (gameData.race.track.pieces[i].angle != null) {
					return distanceToNextCurve;
				}
				distanceToNextCurve += gameData.race.track.pieces[i].length;
			}
			return distanceToNextCurve;
		}

		// Päätellään kappaleen pituus
		public double DeterminePieceLength(int pieceIndex, int startlaneIndex, int endlaneIndex, RudeAI.GI.Data gameData)
		{
			if (gameData.race.track.pieces [pieceIndex].angle != null) {
				var piece = gameData.race.track.pieces [pieceIndex];
				var startlane = gameData.race.track.lanes [startlaneIndex];
				var endlane = gameData.race.track.lanes [endlaneIndex];
				double startlaneDistance = startlane.distanceFromCenter;
				double endlaneDistance = endlane.distanceFromCenter;
				if (piece.angle < 0) {
					startlaneDistance = startlaneDistance * -1;
					endlaneDistance = endlaneDistance * -1;
				}
				if (startlaneIndex != endlaneIndex) {
					// ei toimi näin mutta raja-arvot pelastaa sössimästä koko algoritmia
					return Math.Sqrt ((((piece.radius ?? 0) - startlaneDistance) * ((piece.radius ?? 0) - startlaneDistance)) + (((piece.radius ?? 0) - endlaneDistance) * ((piece.radius ?? 0) - endlaneDistance)));
				} else {
					return (2 * System.Math.PI * ((piece.radius ?? 0) - endlaneDistance)) * ((Math.Abs (piece.angle ?? 0)) / 360);
				}
			} else {
				if (startlaneIndex != endlaneIndex) {
					//double differenceBetweenLanes = Math.Abs (gameData.race.track.lanes [startlaneIndex].distanceFromCenter) + Math.Abs (gameData.race.track.lanes [endlaneIndex].distanceFromCenter);
					return Math.Sqrt ((gameData.race.track.pieces [pieceIndex].length * gameData.race.track.pieces [pieceIndex].length) + (20 * 20));
				} else {
					return gameData.race.track.pieces [pieceIndex].length;
				}
			}
		}

		// Päätellään nopeus (pieceindex/tick)
		public double DetermineSpeed (double previousPosition, double currentPosition, double previousPieceLength)
		{
			if (currentPosition >= previousPosition) {
				return currentPosition - previousPosition;
			} else {
				return previousPieceLength - previousPosition + currentPosition;
			}
		}
		
		// Päätellään etäisyys seuraavaan mutkaan
		public double DetermineDistanceToNextCurve (int pieceIndex, double currentPosition, RudeAI.GI.Data gameData, ref double? angle, ref int? radius)
		{
			double distanceToNextCurve = 0;

			for (int i = pieceIndex; i < gameData.race.track.pieces.Count; i++)
			{
				if (gameData.race.track.pieces[i].angle != null) {
					angle = gameData.race.track.pieces[i].angle;
					radius = gameData.race.track.pieces [i].radius;
					return distanceToNextCurve;
				}
				if (i == pieceIndex) {
					distanceToNextCurve += gameData.race.track.pieces [i].length - currentPosition;
				} else {
					distanceToNextCurve += gameData.race.track.pieces[i].length;
				}
			}
			for (int i = 0; i < pieceIndex; i++) {
				if (gameData.race.track.pieces[i].angle != null) {
					angle = gameData.race.track.pieces[i].angle;
					radius = gameData.race.track.pieces [i].radius;
					return distanceToNextCurve;
				}
				distanceToNextCurve += gameData.race.track.pieces[i].length;
			}
			return distanceToNextCurve;
		}

		// Päätellään autoon kohdistuva keskipakovoima mutkassa
		public double DetermineCornerForce(int pieceIndex, int laneIndex, RudeAI.GI.Data gameData, double speed)
		{
			var piece = gameData.race.track.pieces [pieceIndex];
			var piecelength = piece.radius;
			var lane = gameData.race.track.lanes [laneIndex];
			var laneDistance = lane.distanceFromCenter;
			if (piece.angle < 0)
				laneDistance = laneDistance * -1;
			double keskipakovoima = (speed * speed) / (piece.radius ?? 0 - laneDistance);
			return keskipakovoima;
		}
	}
}


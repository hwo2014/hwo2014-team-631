using System;
using System.Collections.Generic;
using System.Linq;
using RudeAI.Car;
using RudeAI.GI;

namespace RudeAI
{
	public class LaneSwitcher
	{
		public List<LaneChangeData> LaneChanges;
		public int Lanes;

		public void Initialize (Track trackData)
		{
			this.Lanes = trackData.lanes.Count;
			this.LaneChanges = new List<LaneChangeData> ();

			for (int i = 0; i < trackData.pieces.Count; i++) {
				if (trackData.pieces [i].@switch.HasValue && trackData.pieces [i].@switch.Value) {
					this.LaneChanges.Add (new LaneChangeData () {PieceIndex = i});
					Console.Out.WriteLine ("switchable piece " + i);
				}
			}
					
			if (this.LaneChanges.Count < 1) {
				return;
			};

			double combinedAbsAngleToLeftBeforeNextSwitch = 0;
			double combinedAbsAngleToRightBeforeNextSwitch = 0;

			int skip = 1;
			int start = this.LaneChanges.OrderBy(x => x.PieceIndex).First().PieceIndex;
			int end = this.LaneChanges.OrderBy(x => x.PieceIndex).Skip (skip).First().PieceIndex;

			do{

				for (int i = start; i < end; i++) {
					var piece = trackData.pieces [i];
					if (piece.angle.HasValue && piece.angle.Value > 0.0) {
						combinedAbsAngleToRightBeforeNextSwitch += piece.angle.Value;
						Console.WriteLine ("Angle to right at piece: " + i);
					} else if (piece.angle.HasValue && piece.angle.Value < 0.0) {
						combinedAbsAngleToLeftBeforeNextSwitch += Math.Abs (piece.angle.Value);
						Console.WriteLine ("Angle to left at piece: " + i);
					}
				}

				Console.WriteLine ("combined angle to right: " + combinedAbsAngleToRightBeforeNextSwitch);
				Console.WriteLine ("combined angle to left: " + combinedAbsAngleToLeftBeforeNextSwitch);

				if (combinedAbsAngleToRightBeforeNextSwitch > combinedAbsAngleToLeftBeforeNextSwitch) {
					LaneChanges.First (x => x.PieceIndex == start).ChangeTo = LaneChange.Right;
					Console.WriteLine ("Lane change to right at piece " + LaneChanges.First (x => x.PieceIndex == start).PieceIndex);
				} else if (combinedAbsAngleToRightBeforeNextSwitch < combinedAbsAngleToLeftBeforeNextSwitch) {
					LaneChanges.First (x => x.PieceIndex == start).ChangeTo = LaneChange.Left;
					Console.WriteLine ("Lane change to left at piece " + LaneChanges.First (x => x.PieceIndex == start).PieceIndex);
				} else {
					LaneChanges.First (x => x.PieceIndex == start).ChangeTo = LaneChange.NoChange;
					Console.WriteLine ("No lane change at piece " + LaneChanges.First (x => x.PieceIndex == start).PieceIndex);
				}

				start = end;
				if(this.LaneChanges.OrderBy(x => x.PieceIndex).Skip(skip+1).Any())
				{
					end = this.LaneChanges.OrderBy (x => x.PieceIndex).Skip (skip += 1).First ().PieceIndex;
				}
				else{
					break;
				}

				combinedAbsAngleToLeftBeforeNextSwitch = 0;
				combinedAbsAngleToRightBeforeNextSwitch = 0;

			}while(end <= this.LaneChanges.OrderBy(x => x.PieceIndex).Last().PieceIndex);

		}

		public string Change (List<CarPosition> carPositionData, string myCarName, Track trackData, double speed)
		{
			var myCarData = carPositionData.First (x => x.id.name == myCarName);					

			if (this.LaneChanges.Any (x => x.PieceIndex == myCarData.piecePosition.pieceIndex + 1)) {
				var changeTo = this.LaneChanges.OrderBy (x => x.PieceIndex).First (y => y.PieceIndex == myCarData.piecePosition.pieceIndex + 1).ChangeTo;

				if(changeTo == LaneChange.Left || changeTo == LaneChange.Right)
				{
					return changeTo;
				}				
			}
			/*
			double distanceLimit = 200;
			double distanceToNextCar = GetDistanceToNextCar(carPositionData, myCarName, trackData, distanceLimit);

			if (distanceToNextCar < 80)
			{
				int lanes = trackData.lanes.Count;			
				if(myCarData.piecePosition.lane.endLaneIndex == trackData.lanes.Count)
				{
					return LaneChange.Left;
				}
				else
				{
					return LaneChange.Right;
				}
			}*/

			return LaneChange.NoChange;
		}

		// Laskee etäisyyden seuraavaan autoon samalla lanella
		public double GetDistanceToNextCar (List<CarPosition> carPositionData, string myCarName, Track trackData, double distanceLimit)
		{
			if (carPositionData.Count == 1) {
				return 0.0;
			}

			var myCarData = carPositionData.First (x => x.id.name == myCarName);
			var currentLane = myCarData.piecePosition.lane.endLaneIndex;
			double distanceToNextCar = 0;
			double pieceLength = 0;

			for (int i = myCarData.piecePosition.pieceIndex; i < trackData.pieces.Count; i++)
			{
				if (i == myCarData.piecePosition.pieceIndex) {
					var nextCar = carPositionData.Where (x => x.piecePosition.lane.startLaneIndex == currentLane && 
						x.id.name != myCarName && x.piecePosition.inPieceDistance > myCarData.piecePosition.inPieceDistance &&
						x.piecePosition.pieceIndex == i)
						.OrderByDescending (x => x.piecePosition.inPieceDistance).FirstOrDefault ();
					if (nextCar != null) {
						return nextCar.piecePosition.inPieceDistance - myCarData.piecePosition.inPieceDistance;
					}
					if (trackData.pieces [i].angle != null) {
						var laneDistance = trackData.lanes [currentLane].distanceFromCenter;
						laneDistance = laneDistance * -1;
						pieceLength = (2 * System.Math.PI * ((trackData.pieces [i].radius ?? 0) - laneDistance)) * ((Math.Abs (trackData.pieces [i].angle ?? 0)) / 360);
					} else {
						pieceLength = trackData.pieces [i].length;
					}
					distanceToNextCar += pieceLength;
				} else {
					var nextCar = carPositionData.Where (x => x.piecePosition.lane.startLaneIndex == currentLane && x.piecePosition.pieceIndex == i)
						.OrderByDescending (x => x.piecePosition.inPieceDistance).FirstOrDefault ();
					if (nextCar != null) {
						distanceToNextCar += nextCar.piecePosition.inPieceDistance;
						return distanceToNextCar;
					}
					if (trackData.pieces [i].angle != null) {
						var laneDistance = trackData.lanes [currentLane].distanceFromCenter;
						laneDistance = laneDistance * -1;
						pieceLength = (2 * System.Math.PI * ((trackData.pieces [i].radius ?? 0) - laneDistance)) * ((Math.Abs (trackData.pieces [i].angle ?? 0)) / 360);
					} else {
						pieceLength = trackData.pieces [i].length;
					}
					distanceToNextCar += pieceLength;
				}

				if(distanceToNextCar > distanceLimit)
				{
					return 0.0;
				}
			}

			for (int i = 0; i < myCarData.piecePosition.pieceIndex; i++) {
				var nextCar = carPositionData.Where (x => x.piecePosition.lane.startLaneIndex == currentLane && x.piecePosition.pieceIndex == i)
					.OrderByDescending (x => x.piecePosition.inPieceDistance).FirstOrDefault ();
				if (nextCar != null) {
					distanceToNextCar += nextCar.piecePosition.inPieceDistance;
					return distanceToNextCar;
				}
				if (trackData.pieces [i].angle != null) {
					var laneDistance = trackData.lanes [currentLane].distanceFromCenter;
					laneDistance = laneDistance * -1;
					pieceLength = (2 * System.Math.PI * ((trackData.pieces [i].radius ?? 0) - laneDistance)) * ((Math.Abs (trackData.pieces [i].angle ?? 0)) / 360);
				} else {
					pieceLength = trackData.pieces [i].length;
				}
				distanceToNextCar += pieceLength;

				if(distanceToNextCar > distanceLimit)
				{
					return 0.0;
				}
			}
			return distanceToNextCar;
		}
	}

	public class LaneChange
	{
		public const string Left = "Left";
		public const string Right = "Right";
		public const string NoChange = "NoChange";
	}

	public class LaneChangeData
	{
		public int PieceIndex { get; set; }
		public string ChangeTo { get; set; }
		public bool ChangeDone { get; set; }
		public int LaneCount { get; set; }
		public bool ChangeOnlyIfOtherCarInFront { get; set; }
	}
}

